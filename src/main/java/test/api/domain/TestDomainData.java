package test.api.domain;

public record TestDomainData(
        String repositoryType,
        String projectName,
        String javaVersion,
        String springVersion
) {

}
