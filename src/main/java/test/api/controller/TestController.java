package test.api.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import test.api.domain.TestDomainData;
import test.api.service.TestService;

@CrossOrigin(origins = {"http://localhost:3000"})
@RestController
public class TestController {

    private final TestService testService;

    public TestController(TestService testService) {
        this.testService = testService;
    }

    @GetMapping("/getRepositoryData/{testId}")
    ResponseEntity<List<TestDomainData>> getTestData(@PathVariable String testId) {
        List<TestDomainData> repositoryResponseDataList = testService.getTestData(testId);

        if (repositoryResponseDataList == null) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(repositoryResponseDataList, HttpStatus.OK);
    }
}
