package test.api.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import test.api.domain.TestDomainData;

@Service
public class TestService {
    public List<TestDomainData> getTestData(String testId) {
        List testData = new ArrayList<>();

        TestDomainData record1 = new TestDomainData(
                "MAVEN",
                "Test Project 1",
                "1.8",
                "3.0"
        );

        TestDomainData record2 = new TestDomainData(
                "GRADLE",
                "Test Project 2",
                "18",
                "2.5"
        );

        TestDomainData record3 = new TestDomainData(
                "UNKNOWN",
                "Test Project 3",
                "",
                ""
        );

        testData.add(record1);
        testData.add(record2);
        testData.add(record3);

        return testData;
    }
}
